package container.measurements;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.temporal.ChronoUnit;

import static java.time.Instant.now;

/**
 * Created by hll on 26.12.14.
 */
class Lantronix extends Thread {
    private final Socket socket;
    private final Vaisala vaisala;

    public Lantronix(Socket socket, Vaisala vaisala) {
        super("Lantronix");
        this.socket = socket;
        this.vaisala = vaisala;
    }

    public void run() {
        //noinspection Since15
        @SuppressWarnings("Since15") VaisalaRecord current = vaisala.getVaisala(now().truncatedTo(ChronoUnit.SECONDS));
        try (PrintWriter out = new PrintWriter(this.socket.getOutputStream(), true)) {
            do {
                out.println(current.data);
                current = vaisala.getVaisala(current.time);
            } while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}