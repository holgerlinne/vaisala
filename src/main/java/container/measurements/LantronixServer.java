package container.measurements;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by hll on 04.01.15.
 */

class LantronixServer extends Thread {
    private final int port;
    private final Vaisala vaisala;

    public LantronixServer (@SuppressWarnings("SameParameterValue") int port, Vaisala vaisala) {
        super("LantronixServer");
        this.port = port;
        this.vaisala = vaisala;
    }

    public void run() {
        try {
            final ServerSocket serverSocket = new ServerSocket(port);
            do {
                new Lantronix(serverSocket.accept(), vaisala).start();
            } while (true);
        } catch (IOException e) {
            System.err.println("Could not listen on port " + Integer.toString(port));
            System.exit(-1);
        }

    }
}
