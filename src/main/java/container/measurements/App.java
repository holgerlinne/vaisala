package container.measurements;

/**
 * Hello world!
 */
class App {

    public static void main(String[] args) {
        VaisalaRecord record;
        Vaisala vaisala = new Vaisala();
        new LantronixServer(10001, vaisala).start();
        new Postgresql(vaisala).start();
        while (true) {}
    }
}
