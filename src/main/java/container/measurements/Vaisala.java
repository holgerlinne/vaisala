package container.measurements;

import jssc.SerialPort;
import jssc.SerialPortException;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by hll on 20.11.14.
 */
class Vaisala {
    final Object sync = new Object();
    private final Object second = new Object();
    private final AtomicBoolean first = new AtomicBoolean(false);
    private Instant activeTime;
    private String activeLine;
    private SerialPortReader vaisalaReader;

    public Vaisala() {
        VaisalaRecord current;
        String ttyDevice = "/dev/ttyO0";
        SerialPort vaisalaSerialPort = new SerialPort(ttyDevice);
        try {
            vaisalaSerialPort.openPort();
            vaisalaSerialPort.setParams(SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            vaisalaSerialPort.setEventsMask(SerialPort.MASK_RXCHAR);
            vaisalaReader = new SerialPortReader(vaisalaSerialPort, this);
            activeTime = vaisalaReader.activeTime;
            vaisalaSerialPort.addEventListener(vaisalaReader);
        } catch (SerialPortException ex) {
            if (vaisalaSerialPort.isOpened()) {
                try {
                    vaisalaSerialPort.closePort();
                } catch (SerialPortException e) {
                    e.printStackTrace();
                }
            }
            System.err.println("Emergency Closed");
        }
        current = getVaisala(null);
        while (current.data == null) {
            current = getVaisala(current.time);
        }
    }

    public VaisalaRecord getVaisala(Instant oldTime) {
        if (oldTime != null) {
            if (first.compareAndSet(false, true)) {
                synchronized (sync) {
                    //noinspection Since15
                    while (!vaisalaReader.activeTime.isAfter(activeTime)) {
                        try {
                            sync.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                activeTime = vaisalaReader.activeTime;
                activeLine = vaisalaReader.activeLine;
                synchronized (second) {
                    first.set(false);
                    second.notifyAll();
                }
            } else {
                synchronized (second) {
                    try {
                        second.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
        //noinspection Since15
        return new VaisalaRecord(
                activeTime.minusSeconds(activeTime.getEpochSecond() % 10L), activeLine);
    }
}