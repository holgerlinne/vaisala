package container.measurements;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hll on 05.01.15.
 */
@SuppressWarnings("Since15")
class Postgresql extends Thread {
    private final Vaisala vaisala;
    private final Map<Parameters, DescriptiveStatistics> statisticMap = new EnumMap<>(Parameters.class);

    public Postgresql(Vaisala vaisala) {
        super("Postgresql");
        this.vaisala = vaisala;

        for (Parameters key : Parameters.values()) {
            if (key != Parameters.others) {
                DescriptiveStatistics stat = new DescriptiveStatistics(64);
                stat.clear();
                statisticMap.put(key, stat);
            }
        }
    }

    public void run() {
        VaisalaRecord record = vaisala.getVaisala(null);
        do {
            decodeAndAddString(record.data);
            ZonedDateTime zdt = ZonedDateTime.ofInstant(record.time, ZoneId.of("UTC"));
            if (zdt.getMinute() % 10 == 0 && zdt.getSecond() == 0) {
                try {
                    store(record.time);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            record = vaisala.getVaisala(record.time);
        } while (true);
    }

    private void decodeAndAddString(String recordData) {
        Parameters parameter;
        DescriptiveStatistics statistic;
        Pattern pattern;
        Matcher matcher;
        double value;

        pattern = Pattern.compile("(\\w+)=([+-]?\\d+\\.?\\d*)([a-zA-Z#])");
        matcher = pattern.matcher(recordData);
        while (matcher.find()) {
            if (matcher.groupCount() != 3) continue;
            try {
                if (!matcher.group(3).equals("#")) {
                    parameter = Parameters.valueOf(matcher.group(1));
                    value = Double.parseDouble(matcher.group(2));
                } else {
                    parameter = Parameters.others;
                    value = Double.NaN;
                }
            } catch (IllegalArgumentException e) {
                parameter = Parameters.others;
                value = Double.NaN;
            }
            if (parameter != Parameters.others && value != Double.NaN) {
                statistic = statisticMap.get(parameter);
                statistic.addValue(value);
                statisticMap.put(parameter, statistic);
            }
        }
    }

    private void store(Instant recordTime) throws IOException {
        StringBuilder nameString = new StringBuilder("insert into vaisala (time");
        Optional<PreparedStatement> insertStatement = Optional.empty();
        String statement;
        String question = ", ?";
        List<Double> values = new ArrayList<>();

        Timestamp nun = new Timestamp(recordTime.toEpochMilli());
        for (Parameters key : statisticMap.keySet()) {
            if (key == Parameters.others) continue;
            DescriptiveStatistics stat = statisticMap.get(key);
            if (stat.getN() <= 0) continue;
            switch (key) {
                case Sn:
                case Dn:
                    values.add(stat.getMin());
                    nameString.append(", ").append(key.name());
                    break;
                case Sx:
                case Dx:
                case Rp:
                case Hp:
                    values.add(stat.getMax());
                    nameString.append(", ").append(key.name());
                    break;
                case Sm:
                case Pa:
                case Ta:
                case Tp:
                case Ua:
                case Ri:
                case Hi:
                case Th:
                case Vh:
                case Vs:
                case Vr:
                    values.add(stat.getMean());
                    nameString.append(", ").append(key.name());
                    break;
                case Rc:
                case Rd:
                case Hc:
                case Hd:
                    values.add(stat.getSum());
                    nameString.append(", ").append(key.name());
                    break;
                case Dm:
                    values.add(Angles.averageAngles(stat)[0]);
                    nameString.append(", ").append(key.name());
                    break;
                case others:
                    break;
            }
            stat.clear();
            statisticMap.put(key, stat);
        }
        nameString.append(") values (?");
        nameString.append(StringUtils.repeat(question, values.size()));
        nameString.append(")");
        statement = nameString.toString();
        try {
            Optional<Connection> connection = Optional.of(DriverManager.getConnection(
                    "jdbc:postgresql://aerodaq.aero:5432/aero", "online", "really"));
            if (connection.isPresent()) {
                insertStatement = Optional.of(connection.get().prepareStatement(statement));
                if (insertStatement.isPresent()) {
                    insertStatement.get().setTimestamp(1, nun);
                    for (int j = 0; j < values.size(); j++) {
                        insertStatement.get().setBigDecimal(j + 2,
                                BigDecimal.valueOf(values.get(j)));
                    }
                    Path path = Paths.get("/tmpdb.sql");
                    if (path.toFile().exists()) {
                        Statement c = connection.get().createStatement();
                        Files.lines(path).forEach(s -> {
                            try {
                                c.executeUpdate(s);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        });
                        c.close();
                        Files.delete(path);
                    }
                    insertStatement.get().executeUpdate();
                    insertStatement.get().close();
                }
                connection.get().close();
            } else {
                FileUtils.writeStringToFile(new File("/tmpdb.sql"), "ddd\n", true);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Postgresql.class.getName()).log(Level.WARNING,
                    "Got killed by insertStatement ", ex);
        }
    }
}

