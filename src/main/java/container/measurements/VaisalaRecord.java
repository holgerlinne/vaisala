package container.measurements;

import java.time.Instant;

/**
 * Created by hll on 03.01.15.
 */
public class VaisalaRecord {
    final Instant time;
    final String data;
    public VaisalaRecord(Instant time, String data) {
        this.time = time;
        this.data = data;
    }
}
