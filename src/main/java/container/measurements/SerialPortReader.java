package container.measurements;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static java.time.Instant.now;

/**
 * Created by hll on 06.12.14.
 */
class SerialPortReader implements SerialPortEventListener {
    volatile String activeLine;
    volatile Instant activeTime;
    private final SerialPort vaisalaSerialPort;
    private final StringBuilder activeBuffer;
    private final Vaisala vaisala;

    public SerialPortReader(SerialPort vaisalaSerialPort, Vaisala vaisala) {
        this.vaisalaSerialPort = vaisalaSerialPort;
        this.vaisala = vaisala;
        activeBuffer = new StringBuilder();
        activeTime = now().truncatedTo(ChronoUnit.SECONDS);
        activeLine = null;
    }

    public void serialEvent(SerialPortEvent event) {
        if (event.isRXCHAR()) {
            try {
                byte[] buffer = vaisalaSerialPort.readBytes();
                for (byte aBuffer : buffer) {
                    if (aBuffer >= 0x20) {
                        activeBuffer.append((char) (aBuffer & 0x7f));
                    } else {
                        if (aBuffer == 0x0a) {
                            synchronized (vaisala.sync) {
                                activeLine = activeBuffer.toString();
                                activeTime = now().truncatedTo(ChronoUnit.SECONDS);
                                vaisala.sync.notifyAll();
                            }
                            activeBuffer.setLength(0);
                        }
                    }
                }
            } catch (SerialPortException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
}
