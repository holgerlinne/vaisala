package container.measurements;

/**
 * Created by hll on 07.01.15.
 */
public enum Parameters {
    Dn, Dm, Dx, Sn, Sm, Sx, Ta, Tp,
    Ua, Pa, Rc, Rd, Ri, Hc, Hd, Hi,
    Rp, Hp, Th, Vh, Vs, Vr, others
}
