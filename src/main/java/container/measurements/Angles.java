/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package container.measurements;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This Algorithm is mathematically derived from the goal of minimizing 
 * (angle[i] - avgAngle)^2 (where the difference is corrected if necessary), 
 * which makes it a true arithmetic mean of the angles.
 * The Algorithm (written in a quite different style of Java) has been found at
 * <a href="http://stackoverflow.com/questions/491738/how-do-you-calculate-the-average-of-a-set-of-angles/3651941#3651941">
 * this</a> Web-page.
 * 
 * @author hll
 */
class Angles {
    
    private static double varnc (@SuppressWarnings("SameParameterValue") double mean, int n,
                                 double sumX, double sumSqrX) {
        return mean * (n * mean - 2 * sumX) + sumSqrX;
    }
	//with lower correction
    private static double varlc(double mean, int n, double sumX, double sumSqrX, int nc, double sumC) {
        return mean * (n * mean - 2 * sumX) + sumSqrX + 2 * 360 * sumC + 
                nc * (-2 * 360 * mean + 360 * 360);
    }
	//with upper correction
    private static double varuc(double mean, int n, double sumX, double sumSqrX, int nc, double sumC) {
        return mean * (n * mean - 2 * sumX) + sumSqrX - 2 * 360 * sumC + 
                nc * (2 * 360 * mean + 360 * 360);
    }

    /**
     * Calculate the average angle from a list of angles tabulated in the
     * Statistics - structure from the Apache 
     * commons.math - package. It returns the angle(s) avgAngle[] that minimizes 
     * (angle[i] - avgAngle)^2
     * 
     * @param angleList accumulated list of angles to be averaged. This list
     * shall not contain invalid numbers like NaNs. All angles are given in
     * degrees.
     * @return the averaged angles from the Statistics-structure. Returns NaN if no
     * valid angle is given in the list.
     */
    public static double[] averageAngles (DescriptiveStatistics angleList) {
        double [] returnValue;
        if (angleList.getN() > 0) {
            double[] angles = new double [(int) angleList.getN()];
            for (int i=0; i < angles.length; i++) 
                angles[i] = angleList.getElement(i);
            returnValue = averageAngles (angles);
        } else {
            returnValue = new double[1];
            returnValue[0] = Double.NaN;
        }
        return returnValue;
    }
    
    private static double[] averageAngles(double[] angles) {
        double      sumAngles;
        double      sumSqrAngles;
        double[]    lowerAngles;
        double[]    upperAngles;

        List<Double> lowerAnglesList = new LinkedList<>();
        List<Double> upperAnglesList = new LinkedList<>();
        sumAngles = 0;
        sumSqrAngles = 0;
        for (double angle : angles) {
            sumAngles += angle;
            sumSqrAngles += angle * angle;
            if (angle <= 180)
                lowerAnglesList.add (angle);
            else if (angle > 180)
                upperAnglesList.add(angle);
        }
        Collections.sort (lowerAnglesList);
        Collections.sort (upperAnglesList, Collections.reverseOrder());

        lowerAngles = new double[lowerAnglesList.size()];
        Iterator<Double> lowerAnglesIter = lowerAnglesList.iterator();
        for (int i = 0; i < lowerAnglesList.size(); i++)
            lowerAngles[i] = lowerAnglesIter.next();
        upperAngles = new double[upperAnglesList.size()];
        Iterator<Double> upperAnglesIter = upperAnglesList.iterator();
        for (int i = 0; i < upperAnglesList.size(); i++)
            upperAngles[i] = upperAnglesIter.next();

        List<Double> averageAnglesList = new LinkedList<>();
        averageAnglesList.add(180d);
        double variance = varnc (180, angles.length, sumAngles, sumSqrAngles);

        double lowerBound = 180;
        double sumLC = 0;
        for (int i = 0; i < lowerAngles.length; i++) {
            double testAverageAngle = (sumAngles + 360 * i) / angles.length;
            if (testAverageAngle > (lowerAngles[i] + 180))
                testAverageAngle = lowerAngles[i];
            if (testAverageAngle > lowerBound) {
                double testVariance = varlc (testAverageAngle, angles.length,
                        sumAngles, sumSqrAngles, i, sumLC);
                if (testVariance < variance) {
                    averageAnglesList.clear ();
                    averageAnglesList.add (testAverageAngle);
                    variance = testVariance;
                } else if (testVariance == variance)
                    averageAnglesList.add (testAverageAngle);
            }
            lowerBound = lowerAngles[i];
            sumLC += lowerAngles[i];
        }

        double testAverageAngle = (sumAngles + 360*lowerAngles.length) / angles.length;
        if (testAverageAngle < 360 && testAverageAngle > lowerBound) {
            double testVariance = varlc (testAverageAngle, angles.length,
                    sumAngles, sumSqrAngles, lowerAngles.length, sumLC);
            if (testVariance < variance) {
                averageAnglesList.clear ();
                averageAnglesList.add (testAverageAngle);
                variance = testVariance;
            } else if (testVariance == variance)
                averageAnglesList.add(testAverageAngle);
        }

        double upperBound = 180;
        double sumUC = 0;
        for (int i = 0; i < upperAngles.length; i++) {
            testAverageAngle = (sumAngles - 360 * i) / angles.length;
            if (testAverageAngle < upperAngles[i] - 180)
                testAverageAngle = upperAngles[i];
            if (testAverageAngle < upperBound) {
                double testVariance = varuc (testAverageAngle, angles.length,
                        sumAngles, sumSqrAngles, i, sumUC);
                if (testVariance < variance) {
                    averageAnglesList.clear();
                    averageAnglesList.add(testAverageAngle);
                    variance = testVariance;
                } else if (testVariance == variance)
                    averageAnglesList.add(testAverageAngle);
            }
            upperBound = upperAngles[i];
            sumUC += upperBound;
        }

        testAverageAngle = (sumAngles - 360 * upperAngles.length) / angles.length;
        if (testAverageAngle < 0)
            testAverageAngle = 0;
        if (testAverageAngle < upperBound) {
            double testVariance = varuc (testAverageAngle, angles.length,
                    sumAngles, sumSqrAngles, upperAngles.length, sumUC);
            if (testVariance < variance) {
                averageAnglesList.clear();
                averageAnglesList.add(testAverageAngle);
                variance = testVariance;
            } else if (testVariance == variance)
                averageAnglesList.add(testAverageAngle);
        }
        double[] averageAngles = new double[averageAnglesList.size()];
        Iterator<Double> averageAnglesIter = averageAnglesList.iterator();
        for (int i = 0; i < averageAngles.length; i++)
            averageAngles[i] = averageAnglesIter.next();
        return averageAngles;
    }


	public class Class {
	}
 }
